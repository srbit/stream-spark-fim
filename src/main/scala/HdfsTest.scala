import com.google.gson.Gson

import org.apache.spark.sql.SQLContext
import org.apache.spark.sql._
import org.apache.spark.sql.SaveMode
import org.apache.spark.{ SparkContext, SparkConf }
import org.apache.spark.streaming.{ StreamingContext, Seconds }

import org.apache.hadoop.fs._

import java.text.SimpleDateFormat
import java.util.Calendar

import java.io.FileReader
import java.io.FileNotFoundException
import java.io.IOException

import org.joda.time.Days
import com.github.nscala_time.time.Imports._

object HdfsTest {
  case class ngx_case(host: String, clientip: String, time: String, uid: String, location: String, response: String, byte: String, agent: String)

  def main(args: Array[String]) {

    //    if (args.length < 1) {
    //      System.err.println("Usage: HdfsTest <file>")
    //      System.exit(1)
    //    }

    val sparkConf = new SparkConf().setAppName("Log Analyzer Streaming in Scala")
    //    val sparkConf = new SparkConf().setAppName("Log Analyzer Streaming in Scala").setMaster("local[4]")
    //        val sparkConf = new SparkConf().setAppName("Log Analyzer Streaming in Scala").setMaster("spark://HP:7077")
    val sc = new SparkContext(sparkConf)
    val sqlContext = new SQLContext(sc)
    import sqlContext.implicits._

    //    val jdbcDF_MD = sqlContext.read.format("jdbc").options(
    //      Map("url" -> "jdbc:mysql://db-media-1.fimplus-prod.io:3306/hd1media_db",
    //        "driver" -> "com.mysql.jdbc.Driver",
    //        "user" -> "hd1media_log",
    //        "password" -> "nnry6VZjCY4UKF8NyPHp",
    //        "dbtable" -> "Resource_movie")).load()
    //
    //    jdbcDF_MD.registerTempTable("jdbcDF_MD")
    //    import sqlContext.implicits._

//    val dfMd = sqlContext.sql("SELECT jdbcDF_MD.id as movieId, jdbcDF_MD.location FROM jdbcDF_MD where jdbcDF_MD.id IS NOT NULL AND jdbcDF_MD.location IS NOT NULL")

    val ssc = new StreamingContext(sc, Seconds(30))
    //    val lines = ssc.textFileStream(args(0))
    val timeLine = time_hadoop()
    try {

      val lines = ssc.textFileStream("hdfs://pn2:9000/kafka/nginxstream_hadoop/" + timeLine)
      //      val lines = ssc.textFileStream("hdfs://pn2:9000/kafka/nginxstream_hadoop")
      val linesFilter = lines.filter(x => !x.contains("python-requests"))
      //    val lines = ssc.textFileStream("hdfs://ha-cluster/kafka/nginxstream_hadoop/" + timeLine)

      linesFilter.foreachRDD(accessLogs => {
        //      lines.foreachRDD(accessLogs => {
        if (accessLogs.count() == 0) {
          println("No logs received in this time interval")
          //          println(dfMd.first())
        } else {
          val rg = """^\[(\S+) (\S+) (\S+) (\S+) \[([\w:/]+\s[+\-]\d{4})\] "(\S+) (\S+) (\S+)" (\d{3}) (\d+) \"(.*?)\" \"(.*?)\" (\S+) (\S+) (\S+)\]""".r
          println("aaaaaaaaaaaaaaaaaaaaa" + accessLogs.count())
          //          accessLogs.toDF()
          //          val accessLogsFilteredDF = accessLogs.map(x => x).toDF().registerTempTable("accessLogsFiltered")
          val timeLine2 = time_processed()
          //                    val ngxTmp1 = sqlContext.read.json(accessLogs).cache()
          //          val ngxTmp1 = sqlContext.read.json(accessLogs).select("message").toDF().registerTempTable("accessLogsFiltered")
          val ngxTmp1 = sqlContext.read.json(accessLogs).select("message")

          val ngxTmp2 = ngxTmp1.map({ line =>
            line.toString() match {
              case rg(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15) =>
                (n1 + "|" + n2 + "|" + n3 + "|" + n4 + "|" + n5 + "|" + n6 + "|" + n7 + "|" + n8 + "|" + n9 + "|" + n10 + "|" + n11 + "|" + n12 + "|" + n13 + "|" + n14 + "|" + n15)
              case _ => ""
            }
          }).map(line => line.split("\\|")).filter(line => line.length > 2).filter(s => s(6).length() >= 98).filter(line => line(8) != "404").map(
            t => ngx_case(
              t(0), t(1) // ,t(2)
              // ,t(3)
              , t(4).substring(0, 11) // ,t(5)
              , t(6).substring(1, 37), t(6).substring(38, 72) // ,t(7)
              , t(8), t(9) // ,t(10)
              , t(11) // ,t(12)
              // ,t(13)
              // ,t(14)
              )).toDF()

          if (ngxTmp2.count() > 0) {
            println("bababababababa" + ngxTmp2.first())
          }

          ngxTmp2.registerTempTable("accessLogsFiltered")

          //          val topTenPostsLast24Hour = sqlContext.sql("SELECT * FROM accessLogsFiltered")

          //          ngxTmp5.write.mode("overwrite").format("parquet").save("hdfs://pn2:9000/kafka/nginxstream_processed/" + timeLine2)
          ngxTmp2.write.mode("append").format("json").save("hdfs://pn2:9000/kafka/nginxstream_processed/" + timeLine2)
          //          topTenPostsLast24Hour.write.mode("append").format("json").save("hdfs://pn2:9000/kafka/nginxstream_processed/" + timeLine2)
          //          topTenPostsLast24Hour.write.mode("append").format("text").save("hdfs://pn2:9000/kafka/nginxstream_processed/" + timeLine2)
          //        topTenPostsLast24Hour.write.mode("ignore").format("text").save("hdfs://pn2:9000/kafka/nginxstream_processed/" + timeLine)
          //        topTenPostsLast24Hour.write.mode("overwrite").format("text").save("hdfs://ha-cluster/kafka/nginxstream_processed/" + timeLine)

          //          ngxTmp5.save("/user/oracle/rm_logs_batch_output/topTenPostsLast24Hour.parquet", "parquet", SaveMode.Overwrite)      

          //           ngxTmp5.saveAsTextFiles("hdfs://pn2:9000/kafka/nginxstream_processed/" + timeLine2 + ".json")  
        }
      })

    } catch {
      case ex: FileNotFoundException => {
        println("Missing file exception")
      }
      case ex: IOException => {
        println("IO Exception")
      }
    } finally {
      println("Exiting finally...")
    }

    ssc.start()
    ssc.awaitTermination()
  }

  def time_processed(): String = {

    val today = Calendar.getInstance.getTime
    val dateFormat = new SimpleDateFormat("YYYYMMdd")
    val yearFormat = new SimpleDateFormat("YYYY")
    val monthFormat = new SimpleDateFormat("MM")
    val dayFormat = new SimpleDateFormat("dd")
    val hourFormat = new SimpleDateFormat("HH")

    val currentDateFormat = dateFormat.format(today)
    val currentYearFormat = yearFormat.format(today)
    val currentMonthFormat = monthFormat.format(today)
    val currentDayFormat = dayFormat.format(today)
    val currentHourFormat = hourFormat.format(today)

    println(currentYearFormat + "/" + currentMonthFormat + "/" + currentDayFormat + "/" + currentHourFormat)
    val timeResult = currentYearFormat + "/" + currentMonthFormat + "/" + currentDayFormat + "/" + currentHourFormat

    return timeResult
  }

  def time_hadoop(): String = {

    val today = Calendar.getInstance.getTime
    val dateFormat = new SimpleDateFormat("YYYYMMdd")

    val hourFormat = new SimpleDateFormat("HH")

    val currentDateFormat = dateFormat.format(today)

    val currentHourFormat = hourFormat.format(today)

    println(currentDateFormat + "/" + currentHourFormat)
    val timeResult = currentDateFormat + "/" + currentHourFormat

    return timeResult
  }
}

