name := "streaming-example"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
	"org.apache.spark" %% "spark-core" % "1.6.1",
	"org.apache.spark" %% "spark-streaming" % "1.6.1",	
	"org.apache.spark" %% "spark-sql" % "1.6.1"
)

libraryDependencies += "com.github.nscala-time" %% "nscala-time" % "2.12.0"
libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"
libraryDependencies += "com.google.code.gson" % "gson" % "2.6.2"
libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.39"